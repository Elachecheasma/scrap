import urllib.request
import bs4

website = 'https://www.footballtunisien.com/'

def a_s(tr):
    #print(tr.prettify())
    tds = tr.find_all('td')
    #print(tds)
    name = tds[1].find_all('a')
    result = {}
    result['Classement'] = tds[0].text.strip() 
    result['Equipe'] = name[1].text.strip() 
    result['Points'] = tds[8].text.strip()
    #print(rank.text.strip())
    #print(name[1].text.strip())
    #print(points.text.strip())
    return result

def scrap_links():
    url = website + '/saison/tunisie-ligue-professionnelle-1-2020-21/'
    request = urllib.request.urlopen(url)
    html = request.read()
    request.close()
    html_parsed = bs4.BeautifulSoup(html, 'html.parser')
    #print(html_parsed.prettify())
    ligue = html_parsed.find('span', { 'itemprop':'name' })
    h1 = ligue.find_parent()
    table = h1.find_next('table')
    tbody = table.find('tbody')
    trs = tbody.find_all('tr')
    #print(trs)
    classement = []
    for tr in trs :
        result=a_s(tr)
        classement.append(result)
        #result=a_s(tr)
        #print(result)    
    return classement
table = scrap_links()
print('Classement' , 'Equipes'.ljust(30) ,'Points')
for team in table:
    print (team['Classement'].ljust(10), team['Equipe'].ljust(30), team['Points'] )
